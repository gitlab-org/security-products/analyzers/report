package report

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCurrentVersion(t *testing.T) {
	want := Version{Major: 15, Minor: 1, Patch: 4, PreRelease: ""}
	got := CurrentVersion()

	require.Equal(t, want, got)
}

func TestVersion(t *testing.T) {
	t.Run("String", func(t *testing.T) {
		want := "12.34.56"
		got := Version{Major: 12, Minor: 34, Patch: 56}.String()
		require.Equal(t, want, got)
	})

	t.Run("String with PreRelease", func(t *testing.T) {
		want := "12.34.56-rc1"
		got := Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"}.String()
		require.Equal(t, want, got)
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		want := []byte(`"12.34.56"`)
		got, err := Version{Major: 12, Minor: 34, Patch: 56}.MarshalJSON()
		require.NoError(t, err)
		require.Equal(t, want, got)
	})

	t.Run("MarshalJSON with PreRelease", func(t *testing.T) {
		want := []byte(`"12.34.56-rc1"`)
		got, err := Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"}.MarshalJSON()
		require.NoError(t, err)
		require.Equal(t, want, got)
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			Input string
			Want  Version
		}{
			{
				Name:  "Empty",
				Input: `null`,
				Want:  Version{Major: VersionMajor, Minor: VersionMinor, Patch: VersionPatch},
			},
			{
				Name:  "Major",
				Input: `"12"`,
				Want:  Version{Major: 12},
			},
			{
				Name:  "Major.Minor",
				Input: `"12.34"`,
				Want:  Version{Major: 12, Minor: 34},
			},
			{
				Name:  "Major.Minor.Patch",
				Input: `"12.34.56"`,
				Want:  Version{Major: 12, Minor: 34, Patch: 56},
			},
			{
				Name:  "Major.Minor.Patch-Pre",
				Input: `"12.34.56-rc1"`,
				Want:  Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Version
				err := got.UnmarshalJSON([]byte(tc.Input))
				require.NoError(t, err)
				require.Equal(t, tc.Want, got)
			})
		}
	})
}
