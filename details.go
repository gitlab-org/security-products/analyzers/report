package report

import (
	"encoding/json"
)

// Details contains properties which abide by the details attribute of the Secure Report Schemas:
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.1.3/src/vulnerability-details-format.json
type Details map[string]interface{}

// DetailsTextField stores a raw text detail type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.1.3/src/vulnerability-details-format.json#L125
type DetailsTextField struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// DetailsURLField stores a raw url detail type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.1.3/src/vulnerability-details-format.json#L141
type DetailsURLField struct {
	Name string `json:"name"`
	Text string `json:"text"`
	Href string `json:"href"`
}

// DetailsFileLocationField stores a file location detail type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.1.3/src/vulnerability-details-format.json#L248
type DetailsFileLocationField struct {
	FileName  string `json:"file_name"`
	LineStart int    `json:"line_start"`
	LineEnd   int    `json:"line_end,omitempty"`
}

// DetailsCodeFlowNodeField stores a node in a code flow, represented by a file location and a node type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.1.3/src/vulnerability-details-format.json#L295
type DetailsCodeFlowNodeField struct {
	NodeType     string                   `json:"node_type"`
	FileLocation DetailsFileLocationField `json:"file_location"`
}

// DetailsCodeFlowsField stores an array of flows, represented by an ordered array of DetailsCodeFlowNodeField
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.1.3/src/vulnerability-details-format.json#L323
type DetailsCodeFlowsField struct {
	Name  string                       `json:"name"`
	Items [][]DetailsCodeFlowNodeField `json:"items"`
}

// MarshalJSON turns a DetailsTextField into a json object
func (d DetailsTextField) MarshalJSON() ([]byte, error) {
	type rawDetailsTextField DetailsTextField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsTextField
	}{
		"text",
		rawDetailsTextField(d),
	}

	return json.Marshal(typed)
}

// MarshalJSON turns a DetailsURLField into a json object
func (d DetailsURLField) MarshalJSON() ([]byte, error) {
	type rawDetailsURLField DetailsURLField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsURLField
	}{
		"url",
		rawDetailsURLField(d),
	}

	return json.Marshal(typed)
}

// MarshalJSON turns a DetailsFileLocationField into a json object
func (d DetailsFileLocationField) MarshalJSON() ([]byte, error) {
	type rawDetailsFileLocationField DetailsFileLocationField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsFileLocationField
	}{
		"file-location",
		rawDetailsFileLocationField(d),
	}

	return json.Marshal(typed)
}

// MarshalJSON turns a DetailsCodeFlowNodeField into a json object
func (d DetailsCodeFlowNodeField) MarshalJSON() ([]byte, error) {
	type rawDetailsCodeFlowNodeField DetailsCodeFlowNodeField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsCodeFlowNodeField
	}{
		"code-flow-node",
		rawDetailsCodeFlowNodeField(d),
	}

	return json.Marshal(typed)
}

// MarshalJSON turns a DetailsCodeFlowNodeField into a json object
func (d DetailsCodeFlowsField) MarshalJSON() ([]byte, error) {
	type rawDetailsCodeFlowsField DetailsCodeFlowsField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsCodeFlowsField
	}{
		"code-flows",
		rawDetailsCodeFlowsField(d),
	}

	return json.Marshal(typed)
}
