package report

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	log "github.com/sirupsen/logrus"
	logtest "github.com/sirupsen/logrus/hooks/test"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

const expectedTime = "2020-01-25T15:04:05"

func testReport() Report {
	parsedStartTime, _ := time.Parse(timeFormat, expectedTime)
	parsedEndTime, _ := time.Parse(timeFormat, expectedTime)

	startTime := ScanTime(parsedStartTime)
	endTime := ScanTime(parsedEndTime)

	return Report{
		Version: Version{14, 0, 3, ""},
		Vulnerabilities: []Vulnerability{
			{
				Category:   CategoryDependencyScanning,
				Name:       "Vulnerability in io.netty/netty",
				Message:    "Vulnerability in io.netty/netty",
				CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
				Scanner: &Scanner{
					ID:   "find_sec_bugs",
					Name: "Find Security Bugs",
				},
				Location: Location{
					File: "app/pom.xml",
					Dependency: &Dependency{
						Package: Package{
							Name: "io.netty/netty",
						},
						Version: "3.9.1.Final",
					},
				},
				Identifiers: []Identifier{
					CVEIdentifier("CVE-2018-1234"),
				},
			},
		},
		Remediations: []Remediation{
			{
				Fixes: []Ref{
					{
						CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
						ID:         "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3",
					},
				},
				Summary: "Upgrade to netty 3.9.2.Final",
				Diff:    "diff (base64 encoded) placeholder",
			},
		},
		Scan: Scan{
			Analyzer: ScannerDetails{
				ID:      "spotbugs",
				Name:    "Spotbugs",
				Version: "2.1.2",
				URL:     "https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs",
				Vendor: Vendor{
					Name: "GitLab",
				},
			},
			Scanner: ScannerDetails{
				ID:      "find_sec_bugs",
				Name:    "Find Security Bugs",
				Version: "1.0.1",
				URL:     "https://github.com/find-sec-bugs/find-sec-bugs",
				Vendor: Vendor{
					Name: "OWASP",
				},
			},
			PrimaryIdentifiers: []Identifier{
				CVEIdentifier("CVE-2018-1234"),
			},
			Type:      CategoryDependencyScanning,
			StartTime: &startTime,
			EndTime:   &endTime,
			Status:    "success",
		},
	}
}

var testReportJSON = fmt.Sprintf(`{
  "version": "14.0.3",
  "vulnerabilities": [
    {
      "id": "466fba6b35cdb9bd077db9a53584822a1b5fcdf97417e5956c1ab9a1cc697c76",
      "category": "dependency_scanning",
      "name": "Vulnerability in io.netty/netty",
      "message": "Vulnerability in io.netty/netty",
      "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
      "scanner": {
        "id": "find_sec_bugs",
        "name": "Find Security Bugs"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.1.Final"
        }
      },
      "identifiers": [
        {
          "type": "cve",
          "name": "CVE-2018-1234",
          "value": "CVE-2018-1234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
        }
      ]
    }
  ],
  "remediations": [
    {
      "fixes": [
        {
          "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
          "id": "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3"
        }
      ],
      "summary": "Upgrade to netty 3.9.2.Final",
      "diff": "diff (base64 encoded) placeholder"
    }
  ],
  "scan": {
    "analyzer": {
      "id": "spotbugs",
      "name": "Spotbugs",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.1.2"
    },
    "scanner": {
      "id": "find_sec_bugs",
      "name": "Find Security Bugs",
      "url": "https://github.com/find-sec-bugs/find-sec-bugs",
      "vendor": {
        "name": "OWASP"
      },
      "version": "1.0.1"
    },
    "primary_identifiers": [
      {
        "type": "cve",
        "name": "CVE-2018-1234",
        "value": "CVE-2018-1234",
        "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
      }
    ],
    "type": "dependency_scanning",
    "start_time": "%s",
    "end_time": "%s",
    "status": "success"
  }
}`, expectedTime, expectedTime)

var testEmptySASTReport = func() Report {
	r := NewReport()
	return r
}()

var testEmptyReportJSON = fmt.Sprintf(`{
  "version": "%d.%d.%d",
  "vulnerabilities": [],
  "scan": {
    "analyzer": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}`, VersionMajor, VersionMinor, VersionPatch)

var testEmptySASTReportJSON = fmt.Sprintf(`{
  "version": "%d.%d.%d",
  "vulnerabilities": [],
  "scan": {
    "analyzer": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}`, VersionMajor, VersionMinor, VersionPatch)

func TestReport(t *testing.T) {
	testCases := []struct {
		Name string
		Report
		ReportJSON string
	}{
		{
			Name:       "GenericReport",
			Report:     testReport(),
			ReportJSON: testReportJSON,
		},
		{
			Name:       "EmptyReport",
			Report:     NewReport(),
			ReportJSON: testEmptyReportJSON,
		},
		{
			Name:       "EmptySASTReport",
			Report:     testEmptySASTReport,
			ReportJSON: testEmptySASTReportJSON,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			t.Run("MarshalJSON", func(t *testing.T) {
				b, err := json.Marshal(tc.Report)
				require.NoError(t, err)

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()

				want := tc.ReportJSON
				require.JSONEq(t, want, got)
			})

			t.Run("UnmarshalJSON", func(t *testing.T) {
				var got Report
				err := json.Unmarshal([]byte(tc.ReportJSON), &got)
				require.NoError(t, err)
				require.Equal(t, tc.Report, got)
			})
		})
	}

	t.Run("ExcludePaths", func(t *testing.T) {
		report := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R1/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/critical/ckey",
					Location:   Location{File: "api/pom.xml"},
				},
				{
					Name:       "R2/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/critical/ckey",
					Location:   Location{File: "api/pom.xml"},
				},
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
					Location:   Location{File: "web/pom.xml"},
				},
				{
					Name:       "R1/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/low/ckey",
					Location:   Location{File: "model/pom.xml"},
				},
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}}, // excluded
					Summary: "Upgrade dependency to fix R1/critical",
					Diff:    "diff fixing R1/critical",
				},
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
		}

		want := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
					Location:   Location{File: "web/pom.xml"},
				},
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
		}

		isExcluded := func(path string) bool {
			switch path {
			case "api/pom.xml", "model/pom.xml":
				return true
			default:
				return false
			}
		}

		report.ExcludePaths(isExcluded)
		require.Equal(t, want, report)
	})
}

func TestNewReport(t *testing.T) {
	got := NewReport().Version
	want := CurrentVersion()
	require.Equal(t, want, got)
}

func TestTruncateTextFields(t *testing.T) {
	reportWithLongName := Report{
		Vulnerabilities: []Vulnerability{
			{
				Category: CategoryDependencyScanning,
				Name:     "Vulnerability with short name",
			},
			{
				Category: CategoryDependencyScanning,
				Name:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
			},
		},
	}

	reportWithTruncatedName := Report{
		Vulnerabilities: []Vulnerability{
			{
				Category: CategoryDependencyScanning,
				Name:     "Vulnerability with short name",
			},
			{
				Category: CategoryDependencyScanning,
				Name:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolo...",
			},
		},
	}

	reportWithLongName.TruncateTextFields()
	require.Equal(t, reportWithTruncatedName, reportWithLongName)
}

func TestSort(t *testing.T) {
	t.Run("Vulnerabilities", func(t *testing.T) {
		tcs := []struct {
			Name            string
			Vulnerabilities []Vulnerability
			Want            []Vulnerability
		}{
			{
				Name: "it sorts by SeverityLevel when all SeverityLevels differ",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelUnknown, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelHigh, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelLow, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelInfo, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelMedium, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
					{Severity: SeverityLevelHigh, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelMedium, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelLow, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelUnknown, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelInfo, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
				},
			},
			{
				Name: "it sorts by CompareKey when all SeverityLevels are the same",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelCritical, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
					{Severity: SeverityLevelCritical, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
				},
			},
			{
				Name: "it sorts by Location.Dependency.Version when all SeverityLevel and CompareKeys are the same",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
				},
			},
			{
				Name: "it does not sort by Location.Dependency.Version when the Location.Dependency field doesn't exist",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "a.go", LineStart: 6}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "e.go", LineStart: 4}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "b.go", LineStart: 1}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "d.go", LineStart: 7}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "f.go", LineStart: 0}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "c.go", LineStart: 9}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "a.go", LineStart: 6}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "e.go", LineStart: 4}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "b.go", LineStart: 1}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "d.go", LineStart: 7}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "f.go", LineStart: 0}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "c.go", LineStart: 9}},
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				report := Report{
					Vulnerabilities: tc.Vulnerabilities,
				}

				report.Sort()

				require.Equal(t, tc.Want, report.Vulnerabilities)
			})
		}
	})

	t.Run("Remediations", func(t *testing.T) {
		tcs := []struct {
			Name         string
			Remediations []Remediation
			Want         []Remediation
		}{
			{
				Name: "it sorts by Fixes[0].CompareKey",
				Remediations: []Remediation{
					{Summary: "f", Fixes: []Ref{{CompareKey: "f"}, {CompareKey: "z"}}},
					{Summary: "b", Fixes: []Ref{{CompareKey: "b"}, {CompareKey: "d"}}},
					{Summary: "e", Fixes: []Ref{{CompareKey: "e"}}},
					{Summary: "a", Fixes: []Ref{{CompareKey: "a"}, {CompareKey: "l"}}},
					{Summary: "d", Fixes: []Ref{{CompareKey: "d"}}},
					{Summary: "c", Fixes: []Ref{{CompareKey: "c"}, {CompareKey: "e"}}},
				},
				Want: []Remediation{
					{Summary: "a", Fixes: []Ref{{CompareKey: "a"}, {CompareKey: "l"}}},
					{Summary: "b", Fixes: []Ref{{CompareKey: "b"}, {CompareKey: "d"}}},
					{Summary: "c", Fixes: []Ref{{CompareKey: "c"}, {CompareKey: "e"}}},
					{Summary: "d", Fixes: []Ref{{CompareKey: "d"}}},
					{Summary: "e", Fixes: []Ref{{CompareKey: "e"}}},
					{Summary: "f", Fixes: []Ref{{CompareKey: "f"}, {CompareKey: "z"}}},
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				report := Report{
					Remediations: tc.Remediations,
				}

				report.Sort()

				require.Equal(t, tc.Want, report.Remediations)
			})
		}
	})
}

func TestDisableIdentifierReports(t *testing.T) {
	tests := []struct {
		Name           string
		Report         Report
		rulesetPath    string
		analyzer       string
		wantLogs       []*log.Entry
		wantIDs        []Identifier
		wantPrimaryIDs []Identifier
		wantErr        error
	}{
		{
			Name: "Filters both primary and non-primary identifiers",
			Report: Report{
				Vulnerabilities: []Vulnerability{
					{
						Identifiers: []Identifier{
							{
								Type:  "CWE",
								Value: "CWE-1",
							},
							{
								Type:  "CWE",
								Value: "CWE-2",
							},
						},
					},
					{
						Identifiers: []Identifier{
							{
								Type:  "CWE",
								Value: "CWE-3",
							},
							{
								Type:  "CWE",
								Value: "CWE-4",
							},
						},
					},
					{
						Identifiers: []Identifier{
							{
								Type:  "CWE",
								Value: "CWE-5",
							},
							{
								Type:  "CWE",
								Value: "CWE-6",
							},
						},
					},
				},
				Scan: Scan{
					PrimaryIdentifiers: []Identifier{
						{Type: "CWE", Value: "CWE-1"},
						{Type: "CWE", Value: "CWE-3"},
						{Type: "CWE", Value: "CWE-5"},
					},
				},
			},
			rulesetPath: "testdata/sast-ruleset-disable-ids.toml",
			analyzer:    "gosec",
			wantLogs: []*log.Entry{
				{Level: log.InfoLevel, Message: "2/3 vulnerabilities disabled using 2/2 ruleset customizations"},
				{Level: log.DebugLevel, Message: "1 vulnerabilities disabled by customization of CWE-CWE-1"},
				{Level: log.DebugLevel, Message: "1 vulnerabilities disabled by customization of CWE-CWE-6"},
			},
			wantIDs:        []Identifier{{Type: "CWE", Value: "CWE-3"}, {Type: "CWE", Value: "CWE-4"}},
			wantPrimaryIDs: []Identifier{{Type: "CWE", Value: "CWE-3"}},
		},
		{
			Name:        "Nil rulesets do not drop identifiers",
			Report:      testReport(),
			rulesetPath: "testdata/bad-path.toml",
			wantErr: &ruleset.ConfigFileNotFoundError{
				RulesetPath: "testdata/bad-path.toml",
			},
			analyzer: "gosec",
			wantLogs: []*log.Entry{},
			wantIDs: []Identifier{
				{Type: "cve", Name: "CVE-2018-1234", Value: "CVE-2018-1234", URL: "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"},
			},
		},
		{
			Name: "Empty rulesets are handled gracefully",
			Report: Report{
				Vulnerabilities: []Vulnerability{},
			},
			rulesetPath: "testdata/sast-ruleset.toml",
			analyzer:    "gosec",
			wantLogs: []*log.Entry{
				{Level: log.DebugLevel, Message: "No Ids found to disable"},
			},
			wantIDs: []Identifier{},
		},
	}

	// set the log level to debug so we can assert that specific debug log messages are present
	log.SetLevel(log.DebugLevel)
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Enable custom ruleset feature availability
			t.Setenv(ruleset.EnvVarGitlabFeatures, ruleset.GitlabFeatureCustomRulesetsSAST)

			rsConfig, err := ruleset.LoadRelative(test.rulesetPath, test.analyzer)
			if test.wantErr == nil {
				require.NoError(t, err)
			} else {
				require.Equal(t, test.wantErr, err)
			}

			hook := logtest.NewGlobal()
			defer hook.Reset()

			test.Report.FilterDisabledRules(rsConfig)

			requireEqualLogEntries(t, hook.AllEntries(), test.wantLogs)

			for _, v := range test.Report.Vulnerabilities {
				require.Equal(t, test.wantIDs, v.Identifiers)
			}

			if len(test.wantPrimaryIDs) > 0 {
				require.Equal(t, test.wantPrimaryIDs, test.Report.Scan.PrimaryIdentifiers)
			}

			// Note: additional check for bug described
			// https://gitlab.com/gitlab-org/security-products/analyzers/common/-/merge_requests/131
			require.NotNil(t, test.Report.Vulnerabilities)
		})
	}
}

func TestApplyReportOverrides(t *testing.T) {
	// Enable custom ruleset feature availability
	t.Setenv(ruleset.EnvVarGitlabFeatures, ruleset.GitlabFeatureCustomRulesetsSAST)

	// set the log level to debug so we can assert that specific debug log messages are present
	log.SetLevel(log.DebugLevel)

	createVulnerability := func(cwe string, severity SeverityLevel) Vulnerability {
		return Vulnerability{
			Name:        "Original name",
			Message:     "Original message",
			Description: "Original description",
			Severity:    severity,
			Scanner:     &Scanner{ID: "gosec", Name: "Gosec"},
			Identifiers: []Identifier{{Type: "CWE", Value: cwe}},
		}
	}

	report := Report{
		Version: CurrentVersion(),
		Vulnerabilities: []Vulnerability{
			createVulnerability("CWE-79", SeverityLevelMedium),
			createVulnerability("CWE-1", SeverityLevelMedium),
			createVulnerability("CWE-99", SeverityLevelMedium),
			createVulnerability("CWE-100", SeverityLevelMedium),
		},
	}

	want := Report{
		Version: CurrentVersion(),
		Vulnerabilities: []Vulnerability{
			{
				Name:        "OVERRIDDEN name",
				Message:     "OVERRIDDEN message",
				Description: "OVERRIDDEN description",
				Severity:    SeverityLevelCritical,
				Scanner: &Scanner{
					ID:   "gosec",
					Name: "Gosec",
				},
				Identifiers: []Identifier{
					{
						Type:  "CWE",
						Value: "CWE-79",
					},
				},
			},
			createVulnerability("CWE-1", SeverityLevelUnknown),
			createVulnerability("CWE-99", SeverityLevelMedium),
			createVulnerability("CWE-100", SeverityLevelMedium),
		},
	}

	wantLogs := []*log.Entry{
		{Level: log.DebugLevel, Message: "Applying report overrides"},
		{Level: log.DebugLevel, Message: "Severity of invalid not recognized. Ignoring."},
		{Level: log.InfoLevel, Message: "3/4 vulnerabilities overridden using 3/3 ruleset customizations"},
		{Level: log.DebugLevel, Message: "1 vulnerabilities overridden by customization of CWE-CWE-1"},
		{Level: log.DebugLevel, Message: "1 vulnerabilities overridden by customization of CWE-CWE-79"},
		{Level: log.DebugLevel, Message: "1 vulnerabilities overridden by customization of CWE-CWE-99"},
	}

	rsConfig, err := ruleset.LoadRelative("testdata/sast-ruleset-overrides.toml", "gosec")
	require.NoError(t, err)

	hook := logtest.NewGlobal()
	defer hook.Reset()
	report.ApplyReportOverrides(rsConfig)

	requireEqualLogEntries(t, hook.AllEntries(), wantLogs)
	require.Equal(t, want, report)
}

// requireEqualLogEntries checks that logged entries match level and message
func requireEqualLogEntries(t *testing.T, expected []*log.Entry, actual []*log.Entry) {
	t.Helper()
	require.Len(t, expected, len(actual))

	for i, gotLog := range expected {
		require.Equal(t, actual[i].Level, gotLog.Level, "Log levels not equal: %s != %s", actual[i].Level, gotLog.Level)
		require.Equal(t, actual[i].Message, gotLog.Message)
	}
}
