package report

import (
	"sort"

	log "github.com/sirupsen/logrus"
)

// customizationTracker tracks usages of customizations for logging
type customizationTracker struct {
	total int
	byID  map[string]int
}

func newCustomizationTracker() *customizationTracker {
	return &customizationTracker{byID: map[string]int{}}
}

func (tracker *customizationTracker) track(id string) {
	tracker.byID[id]++
	tracker.total++
}

func (tracker *customizationTracker) log(action string, totalVulns, totalCustomizations int) {
	log.Infof("%d/%d vulnerabilities %s using %d/%d ruleset customizations",
		tracker.total, totalVulns,
		action,
		len(tracker.byID), totalCustomizations,
	)

	// when debugging, print details for each ruleset customization applied
	if log.GetLevel() < log.DebugLevel {
		return
	}
	ids := make([]string, 0, len(tracker.byID))
	for id := range tracker.byID {
		ids = append(ids, id)
	}
	// sort the ids to make the log messages deterministic
	sort.Strings(ids)

	for _, id := range ids {
		log.Debugf("%d vulnerabilities %s by customization of %s", tracker.byID[id], action, id)
	}
}
