module gitlab.com/gitlab-org/security-products/analyzers/report/v5

go 1.13

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.9.0
	github.com/xeipuuv/gojsonschema v1.2.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.1
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3 v3.0.0
)

retract [v5.4.0, v5.6.0] // Contains breaking changes and should not be used. See https://gitlab.com/gitlab-org/gitlab/-/issues/497405
