package sarif

import (
	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

var (
	sarifSemgrepExcludingSuppressions = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')",
				Description: "Detected the use of eval(). eval() can be dangerous if used to evaluate\ndynamic content. If this content can be input from outside the program, this\nmay be a code injection vulnerability. Ensure evaluated content is not definable\nby external sources. Consider using safer ast.literal_eval.\n",
				Severity:    4,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/test-ignore-yes.py",
					LineStart: 6,
					LineEnd:   6,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B307", Value: "bandit.B307", URL: ""},
					{Type: "cwe", Name: "CWE-95", Value: "95", URL: "https://cwe.mitre.org/data/definitions/95.html"},
					{Type: "owasp", Name: "A1 - Injection", Value: "A1", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')",
				Description: "Detected the use of eval(). eval() can be dangerous if used to evaluate\ndynamic content. If this content can be input from outside the program, this\nmay be a code injection vulnerability. Ensure evaluated content is not definable\nby external sources. Consider using safer ast.literal_eval.\n",
				Severity:    4,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/test-ignore-yes.py",
					LineStart: 14,
					LineEnd:   16,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B307", Value: "bandit.B307", URL: ""},
					{Type: "cwe", Name: "CWE-95", Value: "95", URL: "https://cwe.mitre.org/data/definitions/95.html"},
					{Type: "owasp", Name: "A1 - Injection", Value: "A1", URL: ""},
				},
			},
		},
		Scan:     report.Scan{},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}

	sarifKicsWithUnknownSeverity = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Name Is Not Snake Case",
				Description: "All names should follow snake case pattern.",
				Severity:    1,
				Scanner:     &report.Scanner{ID: "kics", Name: "kics"},
				Location: report.Location{
					File:      "external_secrets.tf",
					LineStart: 1,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kics_id",
						Name:  "Name Is Not Snake Case",
						Value: "1e434b25-8763-4b00-a5ca-ca03b7abbb66",
						URL:   "https://www.terraform.io/docs/extend/best-practices/naming.html#naming",
					},
				},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}

	sarifKics = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "ALB Listening on HTTP",
				Description: "All Application Load Balancers (ALB) should block connection requests over HTTP",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "kics", Name: "kics"},
				Location: report.Location{
					File:      "aws/services/ElasticLoadBalancing/ELB_Access_Logs_And_Connection_Draining.yaml",
					LineStart: 393,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kics_id",
						Name:  "ALB Listening on HTTP",
						Value: "275a3217-ca37-40c1-a6cf-bb57d245ab32",
						URL:   "https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ec2-elb-listener.html#cfn-ec2-elb-listener-protocol",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Insecure Kubernetes Deployment",
				Description: "The Kubernetes Deployment object with name 'example-app' has a security vulnerability. The image used in this deployment contains a known security issue that could be exploited by attackers to gain unauthorized access or control of the container and potentially the underlying host. It is highly recommended to update the image to a secure version or apply necessary patches to eliminate the security risk. Additionally, make sure to follow Kubernetes security best practices to prevent similar issues in the future.",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "kics", Name: "kics"},
				Location: report.Location{
					File:      "k8s/example.yaml",
					LineStart: 10,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kics_id",
						Name:  "Insecure Kubernetes Deployment",
						Value: "K8S-SEC-1001",
						URL:   "https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ec2-elb-listener.html#cfn-ec2-elb-listener-protocol"},
				},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: "../testdata/sast-ruleset.toml",
		},
	}

	sarifSemgrep = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Use of a Broken or Risky Cryptographic Algorithm",
				Description: "Very Long Message: Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n",
				Severity:    4,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "app/app.py",
					LineStart: 141,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
					{Type: "cwe", Name: "CWE-327", Value: "327", URL: "https://cwe.mitre.org/data/definitions/327.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Inadequate Encryption Strength",
				Description: "An insecure SSL version was detected. TLS versions 1.0, 1.1, and all SSL versions\nare considered weak encryption and are deprecated.\nUse 'ssl.PROTOCOL_TLSv1_2' or higher.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B502.B503", Value: "bandit.B502.B503", URL: ""},
					{Type: "cwe", Name: "CWE-326", Value: "326", URL: "https://cwe.mitre.org/data/definitions/326.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Use of a Broken or Risky Cryptographic Algorithm",
				Description: "Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n",
				Severity:    4,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "app/app.py",
					LineStart: 141,
					LineEnd:   141,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
					{Type: "cwe", Name: "CWE-327", Value: "327", URL: "https://cwe.mitre.org/data/definitions/327.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
		},
		Scan: report.Scan{
			PrimaryIdentifiers: []report.Identifier{
				{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
				{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
				{Type: "semgrep_id", Name: "bandit.B502.B503", Value: "bandit.B502.B503", URL: ""},
				{Type: "semgrep_id", Name: "last-duplicate-rule", Value: "duplicate-id", URL: "https://semgrep.dev/r/last"},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}

	sarifSemgrepShortDescriptionEqualFull = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Use of a Broken or Risky Cryptographic Algorithm",
				Description: "Very Long Message: Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n",
				Severity:    4,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "app/app.py",
					LineStart: 141,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
					{Type: "cwe", Name: "CWE-327", Value: "327", URL: "https://cwe.mitre.org/data/definitions/327.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category: "sast",
				// note: this only differs from sarifSemgrep in the following field:
				Name:        "An insecure SSL version was detected. TLS versions 1.0, 1.1, and all SSL versions\nare considered weak encryption and are deprecated.\nUse 'ssl.PROTOCOL_TLSv1_2' or higher.\n",
				Description: "An insecure SSL version was detected. TLS versions 1.0, 1.1, and all SSL versions\nare considered weak encryption and are deprecated.\nUse 'ssl.PROTOCOL_TLSv1_2' or higher.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B502.B503", Value: "bandit.B502.B503", URL: ""},
					{Type: "cwe", Name: "CWE-326", Value: "326", URL: "https://cwe.mitre.org/data/definitions/326.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Use of a Broken or Risky Cryptographic Algorithm",
				Description: "Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n",
				Severity:    4,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "app/app.py",
					LineStart: 141,
					LineEnd:   141,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
					{Type: "cwe", Name: "CWE-327", Value: "327", URL: "https://cwe.mitre.org/data/definitions/327.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
		},
		Scan: report.Scan{
			PrimaryIdentifiers: []report.Identifier{
				{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
				{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
				{Type: "semgrep_id", Name: "bandit.B502.B503", Value: "bandit.B502.B503", URL: ""},
				{Type: "semgrep_id", Name: "last-duplicate-rule", Value: "duplicate-id", URL: "https://semgrep.dev/r/last"},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}

	sarifSemgrepSecuritySeverity = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Use of a Broken or Risky Cryptographic Algorithm",
				Description: "Very Long Message: Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n",
				// this differs from sarifSemgrep by the Severity field
				Severity: 3,
				Scanner:  &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "app/app.py",
					LineStart: 141,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
					{Type: "cwe", Name: "CWE-327", Value: "327", URL: "https://cwe.mitre.org/data/definitions/327.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Inadequate Encryption Strength",
				Description: "An insecure SSL version was detected. TLS versions 1.0, 1.1, and all SSL versions\nare considered weak encryption and are deprecated.\nUse 'ssl.PROTOCOL_TLSv1_2' or higher.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B502.B503", Value: "bandit.B502.B503", URL: ""},
					{Type: "cwe", Name: "CWE-326", Value: "326", URL: "https://cwe.mitre.org/data/definitions/326.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Use of a Broken or Risky Cryptographic Algorithm",
				Description: "Detected MD5 hash algorithm which is considered insecure. MD5 is not\ncollision resistant and is therefore not suitable as a cryptographic\nsignature. Use SHA256 or SHA3 instead.\n",
				// this differs from sarifSemgrep by the Severity field
				Severity: 3,
				Scanner:  &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "app/app.py",
					LineStart: 141,
					LineEnd:   141,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
					{Type: "cwe", Name: "CWE-327", Value: "327", URL: "https://cwe.mitre.org/data/definitions/327.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 17,
					LineEnd:   18,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
			{
				Category:    "sast",
				Name:        "Improper Certificate Validation",
				Description: "Certificate verification has been explicitly disabled. This\npermits insecure connections to insecure servers. Re-enable\ncertification validation.\n",
				Severity:    6,
				Scanner:     &report.Scanner{ID: "semgrep", Name: "semgrep"},
				Location: report.Location{
					File:      "tests/e2e_zap.py",
					LineStart: 28,
					LineEnd:   29,
				},
				Identifiers: []report.Identifier{
					{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
					{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"},
					{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""},
				},
			},
		},
		Scan: report.Scan{
			PrimaryIdentifiers: []report.Identifier{
				{Type: "semgrep_id", Name: "bandit.B303-1", Value: "bandit.B303-1", URL: ""},
				{Type: "semgrep_id", Name: "bandit.B323", Value: "bandit.B323", URL: "https://foo.com/bar/bandit.B323"},
				{Type: "semgrep_id", Name: "bandit.B502.B503", Value: "bandit.B502.B503", URL: ""},
				{Type: "semgrep_id", Name: "last-duplicate-rule", Value: "duplicate-id", URL: "https://semgrep.dev/r/last"},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}

	sarifGitLabAdvancedSast = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Path traversal vulnerability",
				Description: "path.join(here, 'README.rst')",
				Severity:    1,
				Scanner:     &report.Scanner{ID: "lightz", Name: "Lightz"},
				Location: report.Location{
					File:      "setup.py",
					LineStart: 17,
					LineEnd:   17,
				},
				Identifiers: []report.Identifier{
					{Type: "lightz_id", Name: "python-lang-pathtraversal-file-atomic", Value: "python-lang-pathtraversal-file-atomic"},
				},
				Details: &report.Details{
					"code_flows": &report.DetailsCodeFlowsField{
						Name: "code_flows",
						Items: [][]report.DetailsCodeFlowNodeField{
							[]report.DetailsCodeFlowNodeField{
								{
									NodeType: "source",
									FileLocation: report.DetailsFileLocationField{
										FileName:  "code_samples/python/request_http/cross-function_single-file/request-with-http.py",
										LineStart: 8,
										LineEnd:   8,
									},
								},
								{
									NodeType: "propagation",
									FileLocation: report.DetailsFileLocationField{
										FileName:  "code_samples/python/request_http/cross-function_single-file/request-with-http.py",
										LineStart: 27,
										LineEnd:   27,
									},
								},
								{
									NodeType: "propagation",
									FileLocation: report.DetailsFileLocationField{
										FileName:  "code_samples/python/request_http/cross-function_single-file/request-with-http.py",
										LineStart: 23,
										LineEnd:   23,
									},
								},
								{
									NodeType: "sink",
									FileLocation: report.DetailsFileLocationField{
										FileName:  "setup.py",
										LineStart: 17,
										LineEnd:   17,
									},
								},
							},
						},
					},
				},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}

	sarifGitLabAdvancedSastNoDetails = &report.Report{
		Version: report.Version{Major: 15, Minor: 1, Patch: 4},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    "sast",
				Name:        "Path traversal vulnerability",
				Description: "path.join(here, 'README.rst')",
				Severity:    1,
				Scanner:     &report.Scanner{ID: "lightz", Name: "Lightz"},
				Location: report.Location{
					File:      "setup.py",
					LineStart: 17,
					LineEnd:   17,
				},
				Identifiers: []report.Identifier{
					{Type: "lightz_id", Name: "python-lang-pathtraversal-file-atomic", Value: "python-lang-pathtraversal-file-atomic"},
				},
			},
		},
		Analyzer: "gosec",
		Config: ruleset.Config{
			Path: ".gitlab/sast-ruleset.toml",
		},
	}
)
