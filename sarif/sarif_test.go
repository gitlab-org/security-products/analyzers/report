package sarif

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/xeipuuv/gojsonschema"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

var (
	scanner = report.Scanner{
		ID:   "AnalyzerID",
		Name: "AnalyzerName",
	}
	sastJSONSchemaURL = fmt.Sprintf("https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/v%d.%d.%d/dist/sast-report-format.json", report.VersionMajor, report.VersionMinor, report.VersionPatch)
	schemaLoader      = gojsonschema.NewReferenceLoader(sastJSONSchemaURL)
)

func TestUnsupportedSarifVersion(t *testing.T) {
	minimalSarif := `
		{
			"version": "2.0.0"
		}
	`

	report := strings.NewReader(minimalSarif)
	_, err := TransformToGLSASTReport(report, "/tmp/app", "analyzerID", scanner, nil)

	expected := fmt.Errorf("version for SARIF is 2.0.0, but we only support 2.1.0")

	require.NotNil(t, err)
	require.Equal(t, expected.Error(), err.Error())
}

func TestTransformToGLSASTReport(t *testing.T) {
	tests := []struct {
		name        string
		reportPath  string
		wantReport  *report.Report
		features    string
		scanner     report.Scanner
		rulesetPath string
	}{
		{
			name:       "semgrep sarif with security-severity",
			reportPath: "../testdata/reports/semgrep_security_severity.sarif",
			wantReport: sarifSemgrepSecuritySeverity,
			features:   "sast_fp_reduction",
			scanner:    report.Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "semgrep shortDescription and fullDescription are equal for title example (ensure no panic if CWE can't parse)",
			reportPath: "../testdata/reports/semgrep_short_description_equal_full.sarif",
			wantReport: sarifSemgrepShortDescriptionEqualFull,
			features:   "sast_fp_reduction",
			scanner:    report.Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "semgrep sarif shortDescription for title example",
			reportPath: "../testdata/reports/semgrep_short_description.sarif",
			wantReport: sarifSemgrep,
			features:   "sast_fp_reduction",
			scanner:    report.Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "semgrep sarif example",
			reportPath: "../testdata/reports/semgrep.sarif",
			wantReport: sarifSemgrep,
			features:   "sast_fp_reduction",
			scanner:    report.Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:        "KICS sarif example",
			reportPath:  "../testdata/reports/kics.sarif",
			wantReport:  sarifKics,
			scanner:     report.Scanner{ID: "kics", Name: "kics"},
			rulesetPath: "../testdata/sast-ruleset.toml",
		},
		{
			name:       "KICS sarif with 'unknown' severity",
			reportPath: "../testdata/reports/kics_with_unknown_severity.sarif",
			wantReport: sarifKicsWithUnknownSeverity,
			scanner:    report.Scanner{ID: "kics", Name: "kics"},
		},
		{
			name:       "semgrep sarif excluding suppressions",
			reportPath: "../testdata/reports/semgrep_with_suppressions.sarif",
			wantReport: sarifSemgrepExcludingSuppressions,
			scanner:    report.Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "gitlab advanced sast sarif with code flows",
			reportPath: "../testdata/reports/gl-advanced-sast.sarif",
			wantReport: sarifGitLabAdvancedSast,
			scanner:    report.Scanner{ID: "lightz", Name: "Lightz"},
		},
		{
			name:       "gitlab advanced sast sarif with empty code flows",
			reportPath: "../testdata/reports/gl-advanced-sast.no-details.sarif",
			wantReport: sarifGitLabAdvancedSastNoDetails,
			scanner:    report.Scanner{ID: "lightz", Name: "Lightz"},
		},
	}

	for _, tc := range tests {
		var rsConfig *ruleset.Config
		var err error

		if tc.rulesetPath != "" {
			t.Setenv(ruleset.EnvVarGitlabFeatures, ruleset.GitlabFeatureCustomRulesetsSAST)

			rsConfig, err = ruleset.LoadRelative(tc.rulesetPath, "gosec")

			if err != nil {
				log.Fatalf("failed to load ruleset %s: %s", tc.rulesetPath, err.Error())
			}
		}

		t.Run(tc.name, func(t *testing.T) {
			t.Setenv("GITLAB_FEATURES", tc.features)

			report, err := os.Open(tc.reportPath)
			require.NoError(t, err)
			defer report.Close()

			got, err := TransformToGLSASTReport(report, "/tmp/app", "gosec", tc.scanner, rsConfig)
			require.NoError(t, err)
			err = validateSASTJSONSchema(got)
			require.NoError(t, err)
			require.Equal(t, tc.wantReport, got)
		})
	}
}

func TestToolExecutionNotifications(t *testing.T) {
	tests := []struct {
		path   string
		errMsg string
	}{
		{"../testdata/reports/semgrep_invalid_config.sarif", "tool notification error: InvalidRuleSchemaError Additional properties are not allowed ('pattern-eitherrr' was unexpected)"},
		{"../testdata/reports/semgrep_js_syntax_error.sarif", "Semgrep Core WARN - Syntax error: When running eslint.detect-non-literal-require"},
		{"../testdata/reports/semgrep_nonmatching_nosem.sarif", "found 'nosem' comment with id 'bandit.B502'"},
	}

	for _, tc := range tests {
		t.Run(tc.path, func(t *testing.T) {
			fixture, err := os.Open(tc.path)
			require.NoError(t, err)

			logWriter := bytes.NewBuffer(nil)
			log.SetOutput(logWriter)

			_, err = TransformToGLSASTReport(fixture, "/tmp/app", "analyzerID", scanner, nil)
			require.NoError(t, err)

			logOut := logWriter.String()
			require.Contains(t, logOut, tc.errMsg)
		})
	}
}

func TestRemoveRootPath(t *testing.T) {
	tests := []struct {
		path     string
		rootPath string
		expected string
	}{
		{"/a/b/c/d.foo", "/a/b/", "c/d.foo"},
		{"/a/b/c/d.foo", "/a/b", "c/d.foo"},
		{"/a/b/c/d.foo", "/a/c", "/a/b/c/d.foo"},
		{"a/b/c/d.foo", "/a/b", "c/d.foo"},
	}

	for _, tc := range tests {
		got := removeRootPath(tc.path, tc.rootPath)
		require.Equal(t, tc.expected, got)
	}
}

func TestSeverityParsing(t *testing.T) {
	tests := []struct {
		severity string
		expected report.SeverityLevel
	}{
		{
			"unknown",
			report.SeverityLevelUnknown,
		},
		{
			"  UnKnOwN  ",
			report.SeverityLevelUnknown,
		},
		{
			"asdf",
			report.SeverityLevelCritical,
		},
		{
			"Critical",
			report.SeverityLevelCritical,
		},
		{
			"High",
			report.SeverityLevelHigh,
		},
	}

	for _, tc := range tests {
		r := Rule{
			DefaultConfiguration: struct {
				Level string `json:"level"`
			}{
				Level: "error",
			},
			Properties: RuleProperties{
				SecuritySeverity: tc.severity,
			},
		}

		got := Severity(r)
		require.Equal(t, tc.expected, got)
	}
}

func TestIdentifierRegexes(t *testing.T) {
	tests := []struct {
		identifier string
		expected   report.Identifier
	}{
		{"cwe-127", report.Identifier{Type: "cwe", Name: "CWE-127", Value: "127", URL: "https://cwe.mitre.org/data/definitions/127.html"}},
		{"CWE-127", report.Identifier{Type: "cwe", Name: "CWE-127", Value: "127", URL: "https://cwe.mitre.org/data/definitions/127.html"}},
		{"CWE-295: Improper Certificate Validation", report.Identifier{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"}},
		{"OWASP-A3: Sensitive Data Exposure", report.Identifier{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""}},
		{"TestTag-BRxZy: Some Description", report.Identifier{Type: "testtag", Name: "Some Description", Value: "BRxZy", URL: ""}},
		{"invalid tag", report.Identifier{}},
	}

	for _, tc := range tests {
		r := Rule{
			DefaultConfiguration: struct {
				Level string `json:"level"`
			}{
				Level: "error",
			},
			Properties: RuleProperties{
				SecuritySeverity: "High",
				Tags:             []string{tc.identifier},
			},
		}
		ids := identifiers(r, scanner)
		if strings.EqualFold(tc.identifier, "invalid tag") {
			require.Len(t, ids, 1)
			continue
		}
		require.Len(t, ids, 2)
		require.Equal(t, tc.expected, ids[1])
	}
}

func TestIdentifiersFromTags(t *testing.T) {
	baseIdentifier := report.Identifier{
		Type:  "_id",
		Name:  "RuleName",
		Value: "ID",
	}
	OWASPGenericIdentifier := report.Identifier{
		Type:  "OWASP",
		Name:  "A3 - Sensitive Data Exposure",
		Value: "A3",
	}
	OWASPYearIdentifier := report.Identifier{
		Type:  "OWASP",
		Name:  "A03:2017 - Sensitive Data Exposure",
		Value: "A03:2017",
	}

	tests := []struct {
		name                string
		r                   Rule
		s                   report.Scanner
		expectedIdentifiers []report.Identifier
	}{
		{
			name: "with no secondary identifiers",
			r: Rule{
				ID:   "ID",
				Name: "RuleName",
			},
			s: report.Scanner{},
			expectedIdentifiers: []report.Identifier{
				baseIdentifier,
			},
		},
		{
			name: "with CWE identifier",
			r: Rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: RuleProperties{
					Tags: []string{
						"CWE-295: Improper Certificate Validation",
					},
				},
			},
			s: report.Scanner{},
			expectedIdentifiers: []report.Identifier{
				baseIdentifier,
				report.CWEIdentifier(295),
			},
		},
		{
			name: "with OWASP generic identifier",
			r: Rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: RuleProperties{
					Tags: []string{
						"OWASP-A3: Sensitive Data Exposure",
					},
				},
			},
			s: report.Scanner{},
			expectedIdentifiers: []report.Identifier{
				baseIdentifier,
				OWASPGenericIdentifier,
			},
		},
		{
			name: "with OWASP year identifier",
			r: Rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: RuleProperties{
					Tags: []string{
						"OWASP-A03:2017 - Sensitive Data Exposure",
					},
				},
			},
			s: report.Scanner{},
			expectedIdentifiers: []report.Identifier{
				baseIdentifier,
				OWASPYearIdentifier,
			},
		},
		{
			name: "with OWASP strict identifier",
			r: Rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: RuleProperties{
					Tags: []string{
						"OWASP-A03:2017 - Sensitive Data Exposure",
					},
				},
			},
			s: report.Scanner{},
			expectedIdentifiers: []report.Identifier{
				baseIdentifier,
				OWASPYearIdentifier,
			},
		},
		{
			name: "with multiple secondary identifiers",
			r: Rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: RuleProperties{
					Tags: []string{
						"CWE-295: Improper Certificate Validation",
						"OWASP-A3:Sensitive Data Exposure",
						"OWASP-A03:2017 - Sensitive Data Exposure",
						"OWASP-A3:NotYear - Tag",
						"OWASP:Skippable malformed Tag",
					},
				},
			},
			s: report.Scanner{},
			expectedIdentifiers: []report.Identifier{
				baseIdentifier,
				report.CWEIdentifier(295),
				OWASPGenericIdentifier,
				OWASPYearIdentifier,
				{
					Type:  "OWASP",
					Name:  "A3 - NotYear - Tag",
					Value: "A3",
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			ids := identifiers(tc.r, tc.s)
			require.Len(t, ids, len(tc.expectedIdentifiers))
			for idx, id := range ids {
				require.Equal(t, tc.expectedIdentifiers[idx].Name, id.Name)
			}
		})
	}
}

func validateSASTJSONSchema(report *report.Report) error {
	reportLoader := gojsonschema.NewGoLoader(report)
	result, err := gojsonschema.Validate(schemaLoader, reportLoader)
	if err != nil {
		return err
	}

	if result.Valid() {
		return nil
	}

	var validationErrors []string
	for _, err := range result.Errors() {
		// Ignore missing `scan` fields in the report, such as `scan.start_time` and `scan.scanner.version`, since the
		// `scan` field is currently empty and will be added to the report by the analyzer that imports this package.
		if !(strings.HasPrefix(err.Field(), "scan")) {
			validationErrors = append(validationErrors, err.String())
		}
	}

	if len(validationErrors) > 0 {
		return errors.New(strings.Join(validationErrors, "\n"))
	}
	return nil
}
