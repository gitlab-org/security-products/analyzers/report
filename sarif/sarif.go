package sarif

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

// Report is the serif SAST output format, see https://sarifweb.azurewebsites.net
type Report struct {
	Schema  string `json:"$schema"`
	Version string `json:"version"`
	Runs    []Run  `json:"runs"`
}

// Run represents a scan
type Run struct {
	Invocations []invocation `json:"invocations"`
	Results     []Result     `json:"results"`
	Tool        struct {
		Driver struct {
			Name            string `json:"name"`
			SemanticVersion string `json:"semanticVersion"`
			Rules           []Rule `json:"rules"`
		} `json:"driver"`
	} `json:"tool"`
}

type invocation struct {
	ToolExecutionNotifications []notification `json:"toolExecutionNotifications"`
}

type notification struct {
	Descriptor struct {
		ID string `json:"id"`
	} `json:"descriptor"`
	Level   string `json:"level"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
}

// Rule includes semgrep rule information
type Rule struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription struct {
		Text string `json:"text"`
	} `json:"shortDescription"`
	FullDescription struct {
		Text string `json:"text"`
	} `json:"fullDescription"`
	DefaultConfiguration struct {
		Level string `json:"level"`
	} `json:"defaultConfiguration"`
	Properties RuleProperties `json:"properties"`
	HelpURI    string         `json:"helpUri"`
}

// RuleProperties includes rule metadata
type RuleProperties struct {
	Precision        string   `json:"precision"`
	Tags             []string `json:"tags"`
	SecuritySeverity string   `json:"security-severity"`
}

// Location includes the source-code coordinates
type Location struct {
	PhysicalLocation struct {
		ArtifactLocation struct {
			URI       string `json:"uri"`
			URIBaseID string `json:"uriBaseId"`
		} `json:"artifactLocation"`
		Region struct {
			StartLine   int `json:"startLine"`
			StartColumn int `json:"startColumn"`
			EndLine     int `json:"endLine"`
			EndColumn   int `json:"endColumn"`
		} `json:"region"`
	} `json:"physicalLocation"`
}

type threadFlow struct {
	ID      string `json:"threadFlowId"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
	Locations []struct {
		Message  string   `json:"message"`
		Location Location `json:"location"`
	} `json:"locations"`
}

// Result represents a finding
type Result struct {
	RuleID  string `json:"ruleId"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
	Locations    []Location `json:"locations"`
	Suppressions []struct {
		Kind   string `json:"kind"`             // values= 'inSource', 'external'
		Status string `json:"status,omitempty"` // values= empty,'accepted','underReview','rejected'
		GUID   string `json:"guid,omitempty"`
	} `json:"suppressions,omitempty"`
	CodeFlows []struct {
		Message struct {
			Text string `json:"text"`
		} `json:"message"`
		ThreadFlows []threadFlow `json:"threadFlows"`
	} `json:"codeFlows,omitempty"`
}

// match CWE-XXX only
var cweIDRegex = regexp.MustCompile(`([cC][wW][eE])-(\d{1,4})`)

// match (TYPE)-(ID): (Description)
var tagIDRegex = regexp.MustCompile(`([^-]+)-([^:]+):\s*(.+)`)

// TransformToGLSASTReport will take in a sarif file and output a GitLab SAST Report
func TransformToGLSASTReport(reader io.Reader, rootPath, analyzerID string, scanner report.Scanner, rulesetConfig *ruleset.Config) (*report.Report, error) {
	s := Report{}

	jsonBytes, err := readerToBytes(reader)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonBytes, &s)
	if err != nil {
		return nil, err
	}

	if s.Version != "2.1.0" {
		return nil, fmt.Errorf("version for SARIF is %s, but we only support 2.1.0", s.Version)
	}

	newReport := report.NewReport()
	var allVulns []report.Vulnerability
	allIDs := make([]report.Identifier, 0, len(s.Runs[0].Tool.Driver.Rules))

	idMap := make(map[string]report.Identifier)

	// It is generally expected to only have a single run, but best to parse all as it is a collection.
	for _, run := range s.Runs {
		for _, invocation := range run.Invocations {
			for _, notification := range invocation.ToolExecutionNotifications {
				logNotification(notification)
			}
		}

		vulns, ruleMap, err := transformRun(run, rootPath, scanner)
		if err != nil {
			return nil, err
		}

		allVulns = append(allVulns, vulns...)

		if sastFPReductionFeatEnabled() {
			for _, rule := range ruleMap {
				idMap[rule.ID] = primaryIdentifier(rule, scanner)
			}
		}
	}

	for _, id := range idMap {
		allIDs = append(allIDs, id)
	}

	newReport.Analyzer = analyzerID
	newReport.Vulnerabilities = allVulns

	if rulesetConfig != nil {
		newReport.Config.Path = rulesetConfig.Path
		newReport.Config.Ruleset = rulesetConfig.Ruleset
	} else {
		newReport.Config.Path = ruleset.PathSAST
	}

	if sastFPReductionFeatEnabled() {
		sort.Slice(allIDs, func(i, j int) bool {
			return allIDs[i].Value < allIDs[j].Value
		})
		newReport.Scan.PrimaryIdentifiers = allIDs
	}

	return &newReport, nil
}

func readerToBytes(reader io.Reader) ([]byte, error) {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(reader)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// shouldSuppress indicates if the given finding(`result`) should be included
// in the artifact. This is determined based on the `suppressions` property from
// the sarif output file.
func shouldSuppress(r Result) bool {
	if len(r.Suppressions) == 0 {
		return false
	}
	for _, sup := range r.Suppressions {
		// if one of the suppressions is under review or rejected, include
		// the finding in the artifact.
		if sup.Status == "underReview" || sup.Status == "rejected" {
			return false
		}
	}
	return true
}

func transformRun(r Run, rootPath string, scanner report.Scanner) ([]report.Vulnerability, map[string]Rule, error) {
	ruleMap := make(map[string]Rule)
	for _, rule := range r.Tool.Driver.Rules {
		ruleMap[rule.ID] = rule
	}

	var vulns []report.Vulnerability
	for _, result := range r.Results {
		if shouldSuppress(result) {
			continue
		}
		for _, location := range result.Locations {
			rule := ruleMap[result.RuleID]

			var description string
			if len(result.Message.Text) > report.VulnerabilityDescriptionMaxLengthBytes {
				description = result.Message.Text[:report.VulnerabilityDescriptionMaxLengthBytes]
			} else {
				description = result.Message.Text
			}
			// According to this issue - https://gitlab.com/gitlab-org/gitlab/-/issues/381652
			// we will use short Description as the description attribute in Kics specifically.
			if strings.EqualFold(strings.ToLower(scanner.Name), "kics") {
				description = rule.FullDescription.Text
			}

			lineEnd := location.PhysicalLocation.Region.EndLine

			vulns = append(vulns, report.Vulnerability{
				Description: description,
				Category:    report.CategorySast,
				Name:        name(rule),
				Severity:    Severity(rule),
				Scanner:     &scanner,
				Location: report.Location{
					File:      removeRootPath(location.PhysicalLocation.ArtifactLocation.URI, rootPath),
					LineStart: location.PhysicalLocation.Region.StartLine,
					LineEnd:   lineEnd,
				},
				Identifiers: identifiers(rule, scanner),
				Details:     details(result, rootPath),
			})
		}
	}
	return vulns, ruleMap, nil
}

// Severity converts a sarif severity value to a report severity value
// See: https://docs.oasis-open.org/sarif/sarif/v2.1.0/os/sarif-v2.1.0-os.html#_Toc34317855 for more
// information about the level property. The docs say that when level is not defined, then the value is equal
// to warning.
func Severity(r Rule) report.SeverityLevel {
	// if `security-severity` is available, parse it instead of the `DefaultConfiguration` property
	if r.Properties.SecuritySeverity != "" {
		parsedSeverity := report.ParseSeverityLevel(r.Properties.SecuritySeverity)
		securitySeverity := strings.TrimSpace(strings.ToLower(r.Properties.SecuritySeverity))

		if parsedSeverity != report.SeverityLevelUnknown {
			return parsedSeverity
		}

		// parsed security-severity returns unknown, check if it's actually unknown, or just invalid
		if strings.EqualFold(securitySeverity, "unknown") {
			return report.SeverityLevelUnknown
		}
	}

	// security-severity was invalid or not provided, fall through to the `DefaultConfiguration` check
	switch r.DefaultConfiguration.Level {
	case "error":
		return report.SeverityLevelCritical
	case "warning":
		return report.SeverityLevelMedium
	case "note":
		return report.SeverityLevelInfo
	case "none":
		// This primarily caters for Kics, which tends to map "info" level rules to the "none" SARIF level. This
		// causes a loss in fidelity when we transform the SARIF report into a GL SAST report, resulting in "info"
		// level findings being reported as "unknown" in the repository's Vulnerability Report.
		//
		// By mapping back to info, we're essentially un-doing the logic in Kics.
		//
		// See https://gitlab.com/gitlab-org/gitlab/-/issues/349141#note_1145792336 for details.
		return report.SeverityLevelInfo
	default:
		return report.SeverityLevelMedium
	}
}

func name(r Rule) string {
	// prefer shortDescription field over tag: CWE: <text> for semgrep
	if r.ShortDescription.Text != "" && !strings.EqualFold(r.FullDescription.Text, r.ShortDescription.Text) {
		if len(r.ShortDescription.Text) > report.VulnerabilityNameMaxLengthBytes {
			return r.ShortDescription.Text[:report.VulnerabilityNameMaxLengthBytes-3] + "..."
		}

		return r.ShortDescription.Text
	}

	for _, tag := range r.Properties.Tags {
		splits := strings.Split(tag, ":")
		if strings.HasPrefix(splits[0], "CWE") && len(splits) > 1 {
			return strings.TrimLeft(splits[1], " ")
		}
	}

	// default to full text description
	if len(r.FullDescription.Text) > report.VulnerabilityNameMaxLengthBytes {
		return r.FullDescription.Text[:report.VulnerabilityNameMaxLengthBytes-3] + "..."
	}

	return r.FullDescription.Text
}

func identifiers(r Rule, scanner report.Scanner) []report.Identifier {
	ids := []report.Identifier{
		primaryIdentifier(r, scanner),
	}

	for _, tag := range r.Properties.Tags {

		matches := findTagMatches(tag)
		if matches == nil {
			continue
		}

		switch strings.ToLower(matches[1]) {
		case "cwe":
			cweID, err := strconv.Atoi(matches[2])
			if err != nil {
				log.Errorf("Failure to parse CWE ID: %v\n", err)
				continue
			}

			ids = append(ids, report.CWEIdentifier(cweID))
		case "owasp":
			id := matches[2]
			desc := matches[3]

			components := strings.SplitN(matches[3], "-", 2)
			if len(components) == 2 {
				year := strings.TrimSpace(components[0])
				if _, err := strconv.Atoi(year); err == nil {
					id = id + ":" + year
					desc = strings.TrimSpace(components[1])
				}
			}

			ids = append(ids, report.OWASPTop10Identifier(id, desc))
		default:
			ids = append(ids, report.Identifier{
				Type:  report.IdentifierType(strings.ToLower(matches[1])),
				Name:  matches[3],
				Value: matches[2],
			})
		}
	}

	return ids
}

func findTagMatches(tag string) []string {
	// first try to extract (TAG)-(ID): (Description)
	matches := tagIDRegex.FindStringSubmatch(tag)

	if matches == nil {
		// see if we just have a CWE-(XXX) tag
		matches = cweIDRegex.FindStringSubmatch(tag)
	}

	return matches
}

func primaryIdentifier(r Rule, scanner report.Scanner) report.Identifier {
	return report.Identifier{
		Type:  report.IdentifierType(fmt.Sprintf("%s_id", scanner.ID)),
		Name:  r.Name,
		Value: r.ID,
		URL:   r.HelpURI,
	}
}

func removeRootPath(path, rootPath string) string {
	prefix := strings.TrimSuffix(rootPath, "/") + "/"
	if path[0] != '/' {
		prefix = strings.TrimPrefix(prefix, "/")
	}

	return strings.TrimPrefix(path, prefix)
}

func notificationMsg(notification notification) string {
	return fmt.Sprintf(
		"tool notification %s: %s %s",
		notification.Level,
		notification.Descriptor.ID,
		notification.Message.Text)
}

// https://docs.oasis-open.org/sarif/sarif/v2.1.0/csprd01/sarif-v2.1.0-csprd01.html#_Ref493404972
func logNotification(notification notification) {
	msg := notificationMsg(notification)
	switch notification.Level {
	case "error":
		log.Error(msg)
	case "warning":
		log.Warn(msg)
	case "note":
		log.Info(msg)
	case "none":
		log.Debug(msg)
	default:
		log.Debug(msg)
	}
}

// sastFPReductionFeatEnabled returns true if "sast_fp_reduction" feature flag
// is present in the "GITLAB_FEATURES" env variable
func sastFPReductionFeatEnabled() bool {
	features := os.Getenv("GITLAB_FEATURES")
	return strings.Contains(features, "sast_fp_reduction")
}

func extractCodeFlow(threadFlow threadFlow, rootPath string) []report.DetailsCodeFlowNodeField {
	if len(threadFlow.Locations) == 0 {
		return nil
	}

	flow := make([]report.DetailsCodeFlowNodeField, len(threadFlow.Locations))
	for i, location := range threadFlow.Locations {
		flow[i] = report.DetailsCodeFlowNodeField{
			NodeType: "propagation",
			FileLocation: report.DetailsFileLocationField{
				FileName:  removeRootPath(location.Location.PhysicalLocation.ArtifactLocation.URI, rootPath),
				LineStart: location.Location.PhysicalLocation.Region.StartLine,
				LineEnd:   location.Location.PhysicalLocation.Region.EndLine,
			},
		}
	}

	// The SARIF flow array is ordered. The first item is of type `source`, and the last is of type `sink`
	flow[0].NodeType = "source"
	flow[len(flow)-1].NodeType = "sink"
	return flow
}

func extractCodeFlows(r Result, rootPath string) *report.DetailsCodeFlowsField {
	if len(r.CodeFlows) == 0 {
		return nil
	}
	codeFlows := make([][]report.DetailsCodeFlowNodeField, 0)

	// SARIF `CodeFlows` is an array of `CodeFlow` objects, each holding an array `ThreadFlows`, each holding an array
	// `Locations` holding an actual flow from source to sink.
	// See - https://docs.oasis-open.org/sarif/sarif/v2.1.0/cs01/sarif-v2.1.0-cs01.html#_Toc16012612 for details
	for _, sarifCodeFlowObject := range r.CodeFlows {
		for _, threadFlow := range sarifCodeFlowObject.ThreadFlows {
			if flow := extractCodeFlow(threadFlow, rootPath); flow != nil {
				codeFlows = append(codeFlows, flow)
			}
		}
	}

	if len(codeFlows) > 0 {
		return &report.DetailsCodeFlowsField{Items: codeFlows, Name: "code_flows"}
	}

	return nil
}

func details(r Result, rootPath string) *report.Details {
	if codeFlows := extractCodeFlows(r, rootPath); codeFlows != nil {
		return &report.Details{"code_flows": codeFlows}
	}

	return nil
}
