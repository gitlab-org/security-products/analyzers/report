package report

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type marshallable interface {
	MarshalJSON() ([]byte, error)
}

func TestDetailsMarshalJSON(t *testing.T) {
	cases := []struct {
		name     string
		obj      marshallable
		expected string
	}{
		{
			"text field",
			DetailsTextField{Name: "foo", Value: "bar"},
			`{"type":"text","name":"foo","value":"bar"}`,
		},
		{
			"url field",
			DetailsURLField{Text: "foo", Name: "foo", Href: "test.com"},
			`{"type":"url","name":"foo","text":"foo","href":"test.com"}`,
		},
		{
			"file location",
			DetailsFileLocationField{FileName: "name", LineStart: 3, LineEnd: 4},
			`{"type":"file-location","file_name":"name","line_start":3, "line_end":4}`,
		},
		{
			name: "code flow item",
			obj: DetailsCodeFlowNodeField{NodeType: "sink",
				FileLocation: DetailsFileLocationField{FileName: "name", LineStart: 3, LineEnd: 4}},
			expected: `{"type":"code-flow-node", "node_type": "sink", "file_location": {"type":"file-location",
							"file_name":"name","line_start":3, "line_end":4} }`,
		},
		{
			name: "code flows",
			obj: DetailsCodeFlowsField{
				Name: "code_flows",
				Items: [][]DetailsCodeFlowNodeField{{
					{NodeType: "sink",
						FileLocation: DetailsFileLocationField{FileName: "name1", LineStart: 1, LineEnd: 2}},
					{NodeType: "source",
						FileLocation: DetailsFileLocationField{FileName: "name2", LineStart: 3, LineEnd: 4}},
				}},
			},
			expected: `{"type":"code-flows", "name": "code_flows", "items": [[{"type":"code-flow-node", "node_type": "sink", "file_location":
							{"type":"file-location","file_name":"name1","line_start":1, "line_end":2} }, 
							{"type":"code-flow-node", "node_type": "source", "file_location": {"type":"file-location",
							"file_name":"name2","line_start":3, "line_end":4} }]]}`,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			actual, err := c.obj.MarshalJSON()
			require.NoError(t, err)
			require.JSONEq(t, c.expected, string(actual))
		})
	}
}
