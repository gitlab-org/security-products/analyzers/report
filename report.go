package report

import (
	"fmt"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

const timeFormat = "2006-01-02T15:04:05"

// ScanTime is a custom time type formatted using the timeFormat
type ScanTime time.Time

// Status represents the status of a scan, either `success` or `failure`
type Status string

const (
	// StatusSuccess is the identifier for a successful scan
	StatusSuccess Status = "success"
	// StatusFailure is the identifier for a failed scan
	StatusFailure Status = "failure"
)

// Vendor is the vendor/maintainer of the scanner
type Vendor struct {
	Name string `json:"name"` // The name of the vendor
}

// ScannerDetails contains detailed information about the scanner
type ScannerDetails struct {
	ID      string `json:"id"`            // Unique id that identifies the scanner
	Name    string `json:"name"`          // A human readable value that identifies the scanner, not required to be unique
	URL     string `json:"url,omitempty"` // A link to more information about the scanner
	Vendor  Vendor `json:"vendor"`        // The vendor/maintainer of the scanner
	Version string `json:"version"`       // The version of the scanner
}

// AnalyzerDetails contains detailed information about the analyzer
type AnalyzerDetails = ScannerDetails

// Scan contains the identifying information about a security scanner.
type Scan struct {
	Analyzer           AnalyzerDetails `json:"analyzer"`                      // Analyzer describes the analyzer tool which wraps the scanner
	Scanner            ScannerDetails  `json:"scanner"`                       // Scanner is an Object defining the scanner used to perform the scan
	PrimaryIdentifiers []Identifier    `json:"primary_identifiers,omitempty"` // PrimaryIdentifiers identify all rule identifiers for which scan was executed.
	Type               Category        `json:"type"`                          // Type of the scan (container_scanning, dependency_scanning, dast, sast)
	StartTime          *ScanTime       `json:"start_time,omitempty"`          // StartTime is the time when the scan started
	EndTime            *ScanTime       `json:"end_time,omitempty"`            // EndTime is the time when the scan ended
	Status             Status          `json:"status,omitempty"`              // Status is the status of the scan, either `success` or `failure`. Hardcoded to `success` for now
}

// Report is the output of an analyzer.
type Report struct {
	Version         Version         `json:"version"`
	Vulnerabilities []Vulnerability `json:"vulnerabilities"`
	Remediations    []Remediation   `json:"remediations,omitempty"`
	Scan            Scan            `json:"scan"`
	Analyzer        string          `json:"-"`
	Config          ruleset.Config  `json:"-"`
}

// MarshalJSON converts the ScanTime value into a JSON string with the defined timeFormat
func (st *ScanTime) MarshalJSON() ([]byte, error) {
	return []byte(st.String()), nil
}

// UnmarshalJSON converts the JSON string with the defined timeFormat into a ScanTime value
func (st *ScanTime) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), `"`)

	parsedTime, err := time.Parse(timeFormat, s)
	*st = ScanTime(parsedTime)

	return err
}

func (st *ScanTime) String() string {
	t := time.Time(*st)
	return fmt.Sprintf("%q", t.Format(timeFormat))
}

func (s ScannerDetails) String() string {
	return fmt.Sprintf("%s %s analyzer v%s", s.Vendor.Name, s.Name, s.Version)
}

// TruncateTextFields ensures that the report fields don't exceed the maximum allowed length
func (r *Report) TruncateTextFields() {
	r.truncateNameField()
}

func (r *Report) truncateNameField() {
	for i := range r.Vulnerabilities {
		name := r.Vulnerabilities[i].Name
		if len(name) > VulnerabilityNameMaxLengthBytes {
			r.Vulnerabilities[i].Name = name[:VulnerabilityNameMaxLengthBytes-3] + "..."
		}
	}
}

// Sort the Vulnerabilities and Remediations
func (r *Report) Sort() {
	// sort vulnerabilities by Severity, CompareKey, and Location.Dependency.Version (if available)
	sort.Slice(r.Vulnerabilities, func(i, j int) bool {
		vuln1, vuln2 := r.Vulnerabilities[i], r.Vulnerabilities[j]

		if vuln1.Severity != vuln2.Severity {
			// sort by decreasing severity
			return vuln1.Severity > vuln2.Severity
		}

		// severities are the same, attempt to sort by compare key next
		if vuln1.CompareKey != vuln2.CompareKey {
			return vuln1.CompareKey < vuln2.CompareKey
		}

		// compare keys are the same, finally try to sort by Location.Dependency.Version

		// make sure Location.Dependency is available, since some vulnerabilities (ie SAST) don't have this field
		// TODO: do we want to handle SAST reports? If so, we'll need to support sorting by: file, start_line, end_line, class, method
		if vuln1.Location.Dependency != nil && vuln2.Location.Dependency != nil {
			return vuln1.Location.Dependency.Version < vuln2.Location.Dependency.Version
		}

		return false
	})

	// sort remediations by the CompareKey
	sort.Slice(r.Remediations, func(i, j int) bool {
		return r.Remediations[i].Fixes[0].CompareKey < r.Remediations[j].Fixes[0].CompareKey
	})
}

// ExcludePaths excludes paths from vulnerabilities and remediations
// It takes a function that is true when the given path is excluded.
func (r *Report) ExcludePaths(isExcluded func(string) bool) {
	// filter vulnerabilities
	vulns := []Vulnerability{}
	var rejCompareKeys []string
	for _, vuln := range r.Vulnerabilities {
		if isExcluded(vuln.Location.File) {
			rejCompareKeys = append(rejCompareKeys, vuln.CompareKey)
		} else {
			vulns = append(vulns, vuln)
		}
	}

	if len(rejCompareKeys) > 0 {
		sliceCap := len(rejCompareKeys)
		if len(rejCompareKeys) > 10 {
			sliceCap = 10
		}
		log.Debugf("Excluded %v findings matching path exclusions. First 10: %v", len(rejCompareKeys), rejCompareKeys[:sliceCap])
	}

	r.Vulnerabilities = vulns

	// filter remediations
	rems := []Remediation{}
remloop:
	for _, rem := range r.Remediations {
		for _, ref := range rem.Fixes {
			for _, ckey := range rejCompareKeys {
				if ckey == ref.CompareKey {
					continue remloop
				}
			}
		}
		rems = append(rems, rem)
	}
	r.Remediations = rems
}

// Dedupe removes duplicates from vulnerabilities
func (r *Report) Dedupe() {
	r.Vulnerabilities = Dedupe(r.Vulnerabilities...)
}

// NewReport creates a new report in current version.
func NewReport() Report {
	return Report{
		Version:         CurrentVersion(),
		Vulnerabilities: []Vulnerability{},
		Scan:            Scan{},
	}
}

// FilterDisabledRules removes vulnerabilities and identifiers that have been disabled using rulesets
func (r *Report) FilterDisabledRules(rulesetConfig *ruleset.Config) {
	if rulesetConfig == nil {
		return
	}
	disabledIDs, err := rulesetConfig.DisabledIdentifiers()
	if err != nil {
		handleRulesetError(err)
		return
	}
	if len(disabledIDs) == 0 {
		log.Debug("No Ids found to disable")
		return
	}

	tracker := newCustomizationTracker()

	vulns := []Vulnerability{}
	idents := []Identifier{}
	for _, vuln := range r.Vulnerabilities {
		if vulnerabilityEnabled(vuln, disabledIDs, tracker) {
			vulns = append(vulns, vuln)
			idents = append(idents, vuln.Identifiers[0])
		}
	}

	tracker.log("disabled", len(r.Vulnerabilities), len(disabledIDs))

	r.Vulnerabilities = vulns

	if len(r.Scan.PrimaryIdentifiers) > 0 {
		r.Scan.PrimaryIdentifiers = idents
	}
	return
}

func vulnerabilityEnabled(vuln Vulnerability, disabledIDs map[string]bool, tracker *customizationTracker) bool {
	for _, id := range vuln.Identifiers {
		key := strings.Join([]string{string(id.Type), id.Value}, "-")
		if _, ok := disabledIDs[key]; ok {
			tracker.track(key)
			return false
		}
	}
	return true
}

// ApplyReportOverrides applies customer-supplied rulesets to override vulnerabilities
func (r *Report) ApplyReportOverrides(rulesetConfig *ruleset.Config) {
	if rulesetConfig == nil {
		return
	}

	log.Debug("Applying report overrides")

	identifiersWithOverrides, err := rulesetConfig.IdentifiersWithOverrides()
	if err != nil {
		handleRulesetError(err)
		return
	}
	if len(identifiersWithOverrides) == 0 {
		log.Debug("No Ids found to override")
		return
	}

	tracker := newCustomizationTracker()

	vulns := []Vulnerability{}
	for _, vuln := range r.Vulnerabilities {
		vuln = overrideVulnerability(vuln, identifiersWithOverrides, tracker)
		vulns = append(vulns, vuln)
	}

	tracker.log("overridden", len(r.Vulnerabilities), len(identifiersWithOverrides))

	r.Vulnerabilities = vulns
	return
}

func handleRulesetError(err error) {
	switch err.(type) {
	case *ruleset.NotEnabledError, *ruleset.ConfigFileNotFoundError, *ruleset.ConfigNotFoundError:
		log.Debug(err)
	case *ruleset.InvalidConfig:
		log.Fatal(err)
	default:
		log.Error(err)
	}
}

func overrideVulnerability(vuln Vulnerability, identifiersWithOverrides map[string]ruleset.Ruleset, tracker *customizationTracker) Vulnerability {
	for _, id := range vuln.Identifiers {
		key := strings.Join([]string{string(id.Type), id.Value}, "-")
		if override, ok := identifiersWithOverrides[key]; ok {
			tracker.track(key)
			applyOverride(&vuln, override)
		}
	}
	return vuln
}

func applyOverride(vuln *Vulnerability, override ruleset.Ruleset) {
	if override.Override.Name != "" {
		vuln.Name = override.Override.Name
	}
	if override.Override.Message != "" {
		vuln.Message = override.Override.Message
	}
	if override.Override.Description != "" {
		vuln.Description = override.Override.Description
	}
	if override.Override.Severity != "" {
		parsedSeverityLevel := ParseSeverityLevel(override.Override.Severity)
		if parsedSeverityLevel != SeverityLevelUnknown || strings.ToLower(override.Override.Severity) == "unknown" {
			vuln.Severity = parsedSeverityLevel
		} else {
			log.Debugf("Severity of %s not recognized. Ignoring.", override.Override.Severity)
		}
	}
}
