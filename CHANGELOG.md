# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/vulnerability/-/releases).

## v5.7.0
- Retract `v5.4.0` to `5.6.0` because they contain breaking changes (!101)
- Revert https://gitlab.com/gitlab-org/security-products/analyzers/report/-/merge_requests/92 (!101)
- Remove `MergeReports` function since it's no longer used (!98)
- Log the effects of custom ruleset configuration (!97)

## v5.6.0
- (retracted) Remove `MergeReports` function since it's no longer used (!98)

## v5.5.0
- (retracted) Log the effects of custom ruleset configuration (!97)

## v5.4.1
- (retracted) Retract `v5.4.0` because it contains a breaking change (!94)

## v5.4.0
- (retracted) Add `column_start` and `column_end` to `CodeFlows` field under `Details` (!92)

## v5.3.0
- Create `sarif` package and expose structs and functions (!90)

## v5.2.1
- Set `vulnerabilities[].name` attribute to short description for KICS report (!87)
- Set `vulnerablities[].description` attribute to full description for Kics report (!87)

## v5.2.0
- Update `Vulnerability` to include an optional `CodeFlows` field under `Details` (!86)

## v5.1.0
- Add `TruncateTextFields` function (!79)

## v5.0.0
- Fix support for remote rulesets (!80)

## v4.4.0
- Remove deprecated `dependency_files` attribute from generated reports (!83)

## v4.3.2
- Export max vuln length constants (!78)

## v4.3.1
- Fix `CVSSRating.Vector` JSON field name so that it conforms to security report schema v15.0.7 and newer. (!77)

## v4.3.0
- Add `Identifier.Vendor` function to retrieve the canonical name for the vendor that created the identifier (!75)
- Set default report version to `15.0.7`. This default should have changed as part of `v4.2.0`, but was not included. (!75)

## v4.2.0
- Add `CVSSRating` type (!73)
- Update `Vulnerability` to include an optional `CVSSRatings` field (!73)

## v4.1.5
- Do not fail upon SARIF `toolExecutionNotifications` of level `error` (!52)

## v4.1.4
- Update SARIF parser to use `Name` over `Title` (!70)

## v4.1.3
- Update SARIF parser to use `Title` over deprecated `Message` (!69)

## v4.1.2
- Update `ruleset` module to `v2.0.2` to support loading remote Custom Rulesets (!68)

## v4.1.1
- Increase max length of `Vulnerability.Description` to 1 mebibyte, to align with the definition from `v15.x` Security Report Schema (!67)

## v4.1.0
- Add `omitempty` to the `cve` field for SAST reports (!66)

## v4.0.0
- Default to Security Report Schema version `15-0-4` when generating reports (!61)

## v3.22.2
- Fix panic when SARIF parser CWE-XXX tag does not contain title text and is missing the shortDescription metadata field (!64)

## v3.22.1
- Fix `FilterDisabledRules` to filter `Scan.PrimaryIdentifiers` by non-primary identifiers (!63)

## v3.22.0
- Update SARIF parser to change `CWE` Identifier parsing to not require `: ` separator (!62)

## v3.21.0
- Update SARIF parser to prefer `security-severity` property fields over `DefaultConfiguration` (!59)

## v3.20.0
- Update SARIF parser to prefer `shortDescription` text for semgrep `Name` field instead of parsed `CWE` tag field (!60)

## v3.19.0
- Update `FilterDisabledRules` to filter both `Vulnerabilities` and `Scan.PrimaryIdentifiers` (!58)

## v3.18.0
- Add `OWASPTop10Identifier` type (!53)
- Update SARIF OWASP Identifier generation to handle year-scoped categories (!53)
- Include SARIF OWASP Identifier `Value` in `Name` field (!53)

## v3.17.0
- Update `Report.Sort()` to sort `Vulnerabilities` by `Severity`, `CompareKey`, and `Location.Dependency.Version` (if available) (!51)

## v3.16.0
- Map `none` level SARIF findings to `info` (!50)

## v3.15.5
- Update `testify` to v1.8.0 (!46)

## v3.15.4
- Fix schema validation error when report `dependency_files` is empty (!49)

## v3.15.3
- Downgrade default Security Report Schema version from `15-0-0` to `14-0-4` (!48)
- Do not omit cve field when empty (!48)

## v3.15.2
- Update `common` to v3.2.1 to fix failing `gotestsum` command (!44)

## v3.15.1
- Ensure `Scan.PrimaryIdentifiers` has consistent sort (!40)

## v3.15.0
- Add `Scan.PrimaryIdentifiers` field to the security report (!39)

## v3.14.0
- Default to Security Report Schema version `15-0-0` when generating reports (!41)

## v3.13.0
- Support version `15-0-0` of the Security Report Schemas by omitting fields that are not set when encoding vulnerabilities to JSON (!38)
- Change `Vulnerability.Scanner` to a pointer so that it can be omitted when encoding to JSON (!38)

## v3.12.2
- Update `common` to v3.2.0 (!37)

## v3.12.1
- Add `DetailsURLField` to `Details` (!34)

## v3.12.0
- Change the type of `Scan.Analyzer` to `AnalyzerDetails` (!30)

## v3.11.0
- Add `scan.analyzer` field to the security report (!28)

## v3.10.0
- Change `Details` field so that it can be used by client libraries without having to issue new versions of the report module (!24)
- Move Dependency Scanning `vulnerable_package` field to the Gemnasium analyzer (!24)

## v3.9.2
- Includes `suppressions` property during `.sarif` file serialization. (!26)
- Skips to include the finding in the artifact if a sarif result has `suppressions` array property, unless `suppressions[].status` is `underReview` or `rejected`. (!26)

## v3.9.1
- Add missing `PackageManager` constants for Go, Gradle, Pipenv, and others (!25)

## v3.9.0
- Add `Details` field to `Vulnerability` (!23)
- Add `DependencyVulnerablePackageDetails` field for vulnerable package data (!23)

## v3.8.0
- Implement ruleset overrides (!22)

## v3.7.1
- Drop misleading SARIF transformation warning (!21)

## v3.7.0
- Map sarif rule name to identifier.name (!20)

## v3.6.0
- Add support for sarif translation to gl-sast-report.json (!19)

## v3.5.0
- Add support for GHSA identifier type (!17)

## v3.4.1
- Avoid implicit memory aliasing within for loop (!16)

## v3.4.0
- Add `location` fields and `Category` for Cluster Image Scanning reports (!14)

## v3.3.0
- Omit `report.Remediations` if empty (!13)

## v3.2.1
- Bump `report.Version` to `v14.0.3` match latest report schema (!12)

## v3.2.0
- Add `tracking` to the vulnerability struct for post-analyzer processing (!10)

## v3.1.0
- Add `flags` to the vulnerability struct for post-analyzer processing (!9)

## v3.0.1
### Fix

- Fix module reference for major version bump (!6)

## v3.0.0
### Removed

- Removed unused WASC identifier support (!5)

## v2.1.0
### Changed

- Change report version from 3.0.0 to 14.0.0 (!4)

## v2.0.0
### Changed

- Rename `Issue` struct to `Vulnerability` (!2)

## v1.0.0
### Added

- Add code for implementing security scanners that generate [GitLab Security reports](https://gitlab.com/gitlab-org/security-products/security-report-schemas) (!1)
