# Analyzers Report Library
[![](https://gitlab.com/gitlab-org/security-products/analyzers/report/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/report/commits/master)
[![](https://gitlab.com/gitlab-org/security-products/analyzers/report/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/jobs)

This repository contains Go code for implementing security scanners that generate [GitLab Security reports](https://gitlab.com/gitlab-org/security-products/security-report-schemas).

## Release Process

1. Create and merge an MR which introduces new functionality.

   Ensure an entry exists in the [CHANGELOG.md](CHANGELOG.md) for the new version, for example `v4.3.0`.

1. Create a [new release](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/new) in this project.
   1. For the `Tag name (required)` field, enter the new version from the [CHANGELOG.md](CHANGELOG.md), for example `v4.3.0`, and click on `Create tag v4.3.0`.
   1. For the `Release title` field, enter the new version from the [CHANGELOG.md](CHANGELOG.md), for example `v4.3.0`.
   1. In the `Release notes` field, enter the message from the [CHANGELOG.md](CHANGELOG.md), for example:

      ```markdown
      - Add `Identifier.Vendor` function to retrieve the canonical name for the vendor that created the identifier (!75)
      - Set default report version to `15.0.7`. This default should have changed as part of `v4.2.0`, but was not included. (!75)
      ```

   1. Press the `Create release` button.

## Support

This is an internal package used for developing GitLab-specific features. This package is not meant for public consumption, and breaking changes might be introduced at any time. See [publicly available internal tooling](https://about.gitlab.com/support/statement-of-support#publicly-available-internal-tooling) for more details.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
